package com.grupo2uleam4toa.overmanta.InputOutput;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import com.grupo2uleam4toa.overmanta.Model.lugar;
import retrofit2.http.POST;

public interface ApiService {

    @GET("pokemon/1/")
    Call<ArrayList<lugar>> getLugar();

}
