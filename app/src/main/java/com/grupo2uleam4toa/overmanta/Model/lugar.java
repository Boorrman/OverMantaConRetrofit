package com.grupo2uleam4toa.overmanta.Model;

import java.util.List;

public class lugar {
    private String nombre;
    private int foto;
    private int id;
    private int idtipoLugar;
    private String descripcion;
    private double lat;
    private double lng;

    private List<tipoLugar> listaTLugares;

    public void agregarid(tipoLugar tipolugar){
        this.listaTLugares.add(tipolugar);
    }


    public lugar(int id, int idtipoLugar, int foto, String nombre, String descripcion, double lat, double lng) {
        this.nombre = nombre;
        this.foto = foto;
        this.id = id;
        this.id = idtipoLugar;
        this.descripcion = descripcion;
        this.lat = lat;
        this.lng = lng;
    }

    public int getIdtipoLugar() {
        return idtipoLugar;
    }

    public void setIdtipoLugar(int idtipoLugar) {
        this.idtipoLugar = idtipoLugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public List<tipoLugar> getListaTLugares() {
        return listaTLugares;
    }

    public void setListaTLugares(List<tipoLugar> listaTLugares) {
        this.listaTLugares = listaTLugares;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


}
