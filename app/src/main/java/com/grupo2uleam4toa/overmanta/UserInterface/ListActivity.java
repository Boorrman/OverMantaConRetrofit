package com.grupo2uleam4toa.overmanta.UserInterface;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.grupo2uleam4toa.overmanta.InputOutput.ApiAdapter;
import com.grupo2uleam4toa.overmanta.Model.lugar;
import com.grupo2uleam4toa.overmanta.Model.tipoLugar;
import com.grupo2uleam4toa.overmanta.R;
import com.grupo2uleam4toa.overmanta.UserInterface.Adapter.lugarAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity implements Callback<ArrayList<lugar>> {

    private ArrayList<tipoLugar> TLitems = new ArrayList<>();
    private RecyclerView recycler;
    private  RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    private lugarAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);




        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        mAdapter = new lugarAdapter();
        recycler.setAdapter(mAdapter);



        int opcion = getIntent().getExtras().getInt("opcion");

        switch (opcion){
            case 0:
                Call<ArrayList<lugar>> call = ApiAdapter.getApiService().getLugar();
                call.enqueue(this);
                break;
            case 1:

                break;
            case 2:

                break;
            case 3:

                break;
            case 4:

                break;
        }

    }



    @Override
    public void onResponse(Call<ArrayList<lugar>> call, Response<ArrayList<lugar>> response) {
        if(response.isSuccessful()) {
            ArrayList<lugar> lugar = response.body();
            Log.d("onResponseLugar","tamaño arreglo lugar" + lugar.size());
            mAdapter.setmDataSet(lugar);
        }
    }

    @Override
    public void onFailure(Call<ArrayList<lugar>> call, Throwable t) {

    }
}
