package com.grupo2uleam4toa.overmanta.UserInterface.Adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grupo2uleam4toa.overmanta.Model.lugar;
import com.grupo2uleam4toa.overmanta.R;
import com.grupo2uleam4toa.overmanta.UserInterface.DetailActivity;

import java.util.ArrayList;
import java.util.List;

public class lugarAdapter extends RecyclerView.Adapter<lugarAdapter.lugarViewHolder> {
    private ArrayList<lugar> mDataSet;

    public static class lugarViewHolder extends  RecyclerView.ViewHolder{
        public CardView lugarCard;
        public ImageView imageViewLugar;
        public TextView textViewNombreLugar;


        public lugarViewHolder(View v){
            super(v);
            lugarCard = (CardView) v.findViewById(R.id.activity_list_row);
            imageViewLugar = (ImageView) v.findViewById(R.id.imageViewLugar);
            textViewNombreLugar = (TextView) v.findViewById(R.id.textViewNombreLugar);

        }
    }

    public lugarAdapter(){
        mDataSet = new ArrayList<>();
    }

    public void setmDataSet(ArrayList<lugar> dataSet){
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public lugarAdapter.lugarViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.activity_list_row, viewGroup, false);
        return new lugarViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final lugarViewHolder viewHolder, final int i) {
        viewHolder.imageViewLugar.setImageResource(mDataSet.get(i).getFoto());
        viewHolder.textViewNombreLugar.setText(mDataSet.get(i).getNombre());

        viewHolder.lugarCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("detId",mDataSet.get(i).getId());
                bundle.putInt("detImagen", mDataSet.get(i).getFoto());
                bundle.putString("detNombre", mDataSet.get(i).getNombre());
                bundle.putString("detDescripcion", mDataSet.get(i).getDescripcion());
                bundle.putDouble("detLat",mDataSet.get(i).getLat());
                bundle.putDouble("detLng",mDataSet.get(i).getLng());
                Intent intent = new Intent(v.getContext(), DetailActivity.class);
                intent.putExtras(bundle);
                v.getContext().startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


    public lugarAdapter(ArrayList<lugar> Litems) {
        mDataSet = Litems;
    }
}




