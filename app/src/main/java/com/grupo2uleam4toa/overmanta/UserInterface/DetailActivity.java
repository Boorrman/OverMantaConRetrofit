package com.grupo2uleam4toa.overmanta.UserInterface;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.grupo2uleam4toa.overmanta.R;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    TextView textViewNombreLugar;
    TextView textViewDescripcion;
    ImageView imageViewLugar;

    private GoogleMap mMap;

    double lat = 0.0;
    double lng = 0.0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if (status == ConnectionResult.SUCCESS) {

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.anotherMap);
            mapFragment.getMapAsync(this);
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity) getApplicationContext(), 10);
            dialog.show();
        }


        textViewDescripcion = (TextView) findViewById(R.id.textViewDescripcion);
        textViewNombreLugar = (TextView) findViewById(R.id.textViewNombreLugar);
        imageViewLugar = (ImageView) findViewById(R.id.imageViewLugar);

        textViewNombreLugar.setText(getIntent().getExtras().getString("detNombre"));
        textViewDescripcion.setText(getIntent().getExtras().getString("detDescripcion"));
        imageViewLugar.setImageResource(getIntent().getExtras().getInt("detImagen"));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);

        double Lat = getIntent().getExtras().getDouble("detLat");
        double Lng = getIntent().getExtras().getDouble("detLng");
        String Nombre = getIntent().getExtras().getString("detNombre");

        LatLng zona = new LatLng(Lat, Lng);

        mMap.addMarker(new MarkerOptions().position(zona).title(Nombre)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        float zoomlevel = 15;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zona, zoomlevel));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }else{
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            ActualizarUbicacion(location);
        }
        mMap.setMyLocationEnabled(true);

    }


    private void ActualizarUbicacion(Location location){
        if(location != null){
            lat = location.getLatitude();
            lng = location.getLongitude();
            LatLng latlng= new LatLng (lat,lng);
            mMap.addMarker(new MarkerOptions().position(latlng).title("MI ubicacion"));


        }
    }
}
