package com.grupo2uleam4toa.overmanta.UserInterface;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.grupo2uleam4toa.overmanta.Model.lugar;
import com.grupo2uleam4toa.overmanta.R;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private List<lugar> items;

    ImageView imageViewPrincipal;
    TextView textViewPrincipal;
    private static int PETICION_PERMISO_LOCALIZACION = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        imageViewPrincipal = (ImageView) findViewById(R.id.imageViewPrincipal);
        textViewPrincipal = (TextView) findViewById(R.id.textViewPrincipal);



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PETICION_PERMISO_LOCALIZACION);
            return;
        }


    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        int opcion = 0;
        if (id == R.id.lugares_turisticos) {
            opcion = 0;
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra("opcion", opcion);
            startActivity(intent);
        } else if (id == R.id.playas) {
            opcion = 1;
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra("opcion", opcion);
            startActivity(intent);
        } else if (id == R.id.restaurantes) {
            opcion = 2;
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra("opcion", opcion);
            startActivity(intent);
        } else if (id == R.id.bares) {
            opcion = 3;
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra("opcion", opcion);
            startActivity(intent);
        } else if (id == R.id.hoteles) {
            opcion = 4;
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra("opcion", opcion);
            startActivity(intent);
        }else if (id == R.id.mapa){
            Intent intent = new Intent(MainActivity.this, MapsActivity.class);
            intent.putExtra("Lat",-0.967115);
            intent.putExtra("Lng",-80.710202);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
